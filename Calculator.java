import java.util.Scanner;
//We will need to use Scanner to handle user input.

//Overview of the algorithm:
//Input: 2 or 3 arcana types
//Data Known: Using 2d arrays, we
//can have a database for both
//double and triple fusions.
//Procedure: After receiving the user
//input for the 2 or 3 arcana types
//and ensuring the input was valid,
//check if a triple fusion will be calculated.
//If not, only the double fusion table will be
//created. Otherwise, both tables will be created.
//Afterwards, it is a matter of looking up information
//in the tables based on the input.
//Output: The arcana type that will result from the
//fusion of the input.

public class Calculator {

	public static void main(String[] args) {
     
		//We shall treat the arcanas as integers with the 
		//fool arcana being 0, the magician being 1, etc.
		//The 2d integer array will have information for 
		//the results of the arcana fusions.
		//DoubleFusionTable will contain information
		//for double fusions, while 
		//TripleFusionTable will contain
		//information for triple fusions
		int [][] DoubleFusionTable = new int [22][22];
		int [][] TripleFusionTable = new int [22][22];
		//The user shall input the integers corresponding to the 
		//arcanas to be fused.
		Scanner arc_one, arc_two, arc_three;                     
		//String variables are going to be used to parsing the input.
		String stringOne, stringTwo, stringThree;
		//Integer variables are the parsed integers of the 
		//user input using the parseint function.
		//They will be used to locate information in the 
		//2d array.
		int intOne, intTwo, intThree, numOfArcana = 0, result;
		Calculator myCalc = new Calculator();
		//Print out the mapping between integers and arcana.
		System.out.println("Welcome to the Persona 3 Portable Fusion Calculator.\nHere are the numbers corresponding "
				+ "to each arcana:\n0: Fool \n1: Magician \n2: Priestess \n3: Empress \n4: Emperor \n5: Hierophant \n6: Lovers \n"
				+ "7: Chariot\n8: Justice\n9: Hermit\n10: Fortune\n11: Strength\n12: Hanged Man\n13: Death\n14: Temperance"
				+ "\n15: Devil\n16: Tower\n17: Star\n18: Moon\n19: Sun\n20: Judgement\n21: Aeon\n22: No Arcana");
		//We convert the string input into integers and check for bad input such as a or 22.
		//Handling the first input.
		arc_one = new Scanner(System.in);
		stringOne = arc_one.next();
		//Check if we have valid input (numbers 0-21). If not, state error message.
		if(!stringOne.contentEquals("0") & !stringOne.contentEquals("1") & !stringOne.contentEquals("2") & 
				!stringOne.contentEquals("3") & !stringOne.contentEquals("4") & !stringOne.contentEquals("5") &
				!stringOne.contentEquals("6") & !stringOne.contentEquals("7") & !stringOne.contentEquals("8") &
				!stringOne.contentEquals("9") & !stringOne.contentEquals("10") & !stringOne.contentEquals("11") &
				!stringOne.contentEquals("12") & !stringOne.contentEquals("13") & !stringOne.contentEquals("14") &
				!stringOne.contentEquals("15") & !stringOne.contentEquals("16") & !stringOne.contentEquals("17") &
				!stringOne.contentEquals("18") & !stringOne.contentEquals("19") & !stringOne.contentEquals("20") &
				!stringOne.contentEquals("21") & !stringOne.contentEquals("22")){
			System.out.println("Error: Bad input for first arcana. Exitting Persona 3 Portable Fusion Calculator.");
			System.exit(0);
		}
		intOne = Integer.parseInt(stringOne);
		//Handling the second input.
		arc_two = new Scanner(System.in);
		stringTwo = arc_two.next();
		//Check if we have valid input (numbers 0-21). If not, state error message.
		if(!stringTwo.contentEquals("0") & !stringTwo.contentEquals("1") & !stringTwo.contentEquals("2") & 
				!stringTwo.contentEquals("3") & !stringTwo.contentEquals("4") & !stringTwo.contentEquals("5") &
				!stringTwo.contentEquals("6") & !stringTwo.contentEquals("7") & !stringTwo.contentEquals("8") &
				!stringTwo.contentEquals("9") & !stringTwo.contentEquals("10") & !stringTwo.contentEquals("11") &
				!stringTwo.contentEquals("12") & !stringTwo.contentEquals("13") & !stringTwo.contentEquals("14") &
				!stringTwo.contentEquals("15") & !stringTwo.contentEquals("16") & !stringTwo.contentEquals("17") &
				!stringTwo.contentEquals("18") & !stringTwo.contentEquals("19") & !stringTwo.contentEquals("20") &
				!stringTwo.contentEquals("21") & !stringTwo.contentEquals("22")){
			System.out.println("Error: Bad input for second arcana. Exitting Persona 3 Portable Fusion Calculator.");
			System.exit(0);
		}
		intTwo = Integer.parseInt(stringTwo);
		//Handling the third input. This input decides
		//if we are doing a double fusion or a triple
		//fusion.
		arc_three = new Scanner(System.in);
		stringThree = arc_three.next();
		//Check if we have valid input (numbers 0-21). If not, state error message.
		if(!stringThree.contentEquals("0") & !stringThree.contentEquals("1") & !stringThree.contentEquals("2") & 
				!stringThree.contentEquals("3") & !stringThree.contentEquals("4") & !stringThree.contentEquals("5") &
				!stringThree.contentEquals("6") & !stringThree.contentEquals("7") & !stringThree.contentEquals("8") &
				!stringThree.contentEquals("9") & !stringThree.contentEquals("10") & !stringThree.contentEquals("11") &
				!stringThree.contentEquals("12") & !stringThree.contentEquals("13") & !stringThree.contentEquals("14") &
				!stringThree.contentEquals("15") & !stringThree.contentEquals("16") & !stringThree.contentEquals("17") &
				!stringThree.contentEquals("18") & !stringThree.contentEquals("19") & !stringThree.contentEquals("20") &
				!stringThree.contentEquals("21") & !stringThree.contains("22")){
			System.out.println("Error: Bad input for third arcana. Exitting Persona 3 Portable Fusion Calculator.");
			System.exit(0);
		}
		intThree = Integer.parseInt(stringThree);
        //The next step is to determine the type of fusion to calculate. 
		//We will check how many of the arcana are not "no arcana".
		//After knowing how many arcana we have, we can determine what
		//type of fusion will occur or if we actually cannot have a 
		//fusion.
		//Find out how many arcana are being used in the fusion.
		
		if(intOne != 22)
			numOfArcana++;
		if(intTwo != 22)
			numOfArcana++;
		if(intThree != 22)
			numOfArcana++;
		//end of numOfArcana calculation
		//check for 0 or 1 arcana inputted
		
		if(numOfArcana == 0 || numOfArcana == 1)
			System.out.println("No fusion possible. Exitting Persona 3 Portable Fusion Calculator.");
		//check for double fusion
		
		if(numOfArcana == 2) {
			//double fusion calculation
		    DoubleFusionTable = myCalc.SetDoubleFusionTable();
		    result = myCalc.CalcDoubleFusion(intOne, intTwo, intThree, DoubleFusionTable);
		    if(result == 0)
		    	System.out.println("This fusion will result in a persona of the Fool arcana.");
		    else if(result == 1)
		    	System.out.println("This fusion will result in a persona of the Magician arcana.");
		    else if(result == 2)
		    	System.out.println("This fusion will result in a persona of the Priestess arcana.");
		    else if(result == 3)
		    	System.out.println("This fusion will result in a persona of the Empress arcana.");
		    else if(result == 4)
		    	System.out.println("This fusion will result in a persona of the Emperor arcana.");
		    else if(result == 5)
		    	System.out.println("This fusion will result in a persona of the Hierophant arcana.");
		    else if(result == 6)
		    	System.out.println("This fusion will result in a persona of the Lovers arcana.");
		    else if(result == 7)
		    	System.out.println("This fusion will result in a persona of the Chariot arcana.");
		    else if(result == 8)
		    	System.out.println("This fusion will result in a persona of the Justice arcana.");
		    else if(result == 9)
		    	System.out.println("This fusion will result in a persona of the Hermit arcana.");
		    else if(result == 10)
		    	System.out.println("This fusion will result in a persona of the Fortune arcana.");
		    else if(result == 11)
		    	System.out.println("This fusion will result in a persona of the Strength arcana.");
		    else if(result == 12)
		    	System.out.println("This fusion will result in a persona of the Hanged Man arcana.");
		    else if(result == 13)
		    	System.out.println("This fusion will result in a persona of the Death arcana.");
		    else if(result == 14)
		    	System.out.println("This fusion will result in a persona of the Temperance arcana.");
		    else if(result == 15)
		    	System.out.println("This fusion will result in a persona of the Devil arcana.");
		    else if(result == 16)
		    	System.out.println("This fusion will result in a persona of the Tower arcana.");
		    else if(result == 17)
		    	System.out.println("This fusion will result in a persona of the Star arcana.");
		    else if(result == 18)
		    	System.out.println("This fusion will result in a persona of the Moon arcana.");
		    else if(result == 19)
		    	System.out.println("This fusion will result in a persona of the Sun arcana.");
		    else if(result == 20)
		    	System.out.println("This fusion will result in a persona of the Judgment arcana.");
		    else if(result == 21)
		    	System.out.println("This fusion will result in a persona of the Aeon arcana.");
		    
		}
		
		if(numOfArcana == 3)
			//triple fusion calculation
			DoubleFusionTable = myCalc.SetDoubleFusionTable();
		    TripleFusionTable = myCalc.setTripleFusionTable();
		    result = myCalc.CalcTripleFusion(intOne, intTwo, intThree, DoubleFusionTable, TripleFusionTable);
		 if(result == 0)
		    	System.out.println("This fusion will result in a persona of the Fool arcana.");
		    else if(result == 1)
		    	System.out.println("This fusion will result in a persona of the Magician arcana.");
		    else if(result == 2)
		    	System.out.println("This fusion will result in a persona of the Priestess arcana.");
		    else if(result == 3)
		    	System.out.println("This fusion will result in a persona of the Empress arcana.");
		    else if(result == 4)
		    	System.out.println("This fusion will result in a persona of the Emperor arcana.");
		    else if(result == 5)
		    	System.out.println("This fusion will result in a persona of the Hierophant arcana.");
		    else if(result == 6)
		    	System.out.println("This fusion will result in a persona of the Lovers arcana.");
		    else if(result == 7)
		    	System.out.println("This fusion will result in a persona of the Chariot arcana.");
		    else if(result == 8)
		    	System.out.println("This fusion will result in a persona of the Justice arcana.");
		    else if(result == 9)
		    	System.out.println("This fusion will result in a persona of the Hermit arcana.");
		    else if(result == 10)
		    	System.out.println("This fusion will result in a persona of the Fortune arcana.");
		    else if(result == 11)
		    	System.out.println("This fusion will result in a persona of the Strength arcana.");
		    else if(result == 12)
		    	System.out.println("This fusion will result in a persona of the Hanged Man arcana.");
		    else if(result == 13)
		    	System.out.println("This fusion will result in a persona of the Death arcana.");
		    else if(result == 14)
		    	System.out.println("This fusion will result in a persona of the Temperance arcana.");
		    else if(result == 15)
		    	System.out.println("This fusion will result in a persona of the Devil arcana.");
		    else if(result == 16)
		    	System.out.println("This fusion will result in a persona of the Tower arcana.");
		    else if(result == 17)
		    	System.out.println("This fusion will result in a persona of the Star arcana.");
		    else if(result == 18)
		    	System.out.println("This fusion will result in a persona of the Moon arcana.");
		    else if(result == 19)
		    	System.out.println("This fusion will result in a persona of the Sun arcana.");
		    else if(result == 20)
		    	System.out.println("This fusion will result in a persona of the Judgment arcana.");
		    else if(result == 21)
		    	System.out.println("This fusion will result in a persona of the Aeon arcana.");
		
		
		return;


	}
	
	//Calculate Double Fusions using the the double fusion data  
	int CalcDoubleFusion(int intOne, int intTwo, int intThree, int [][] DoubleFusionTable) {
		int result;
		//System.out.println("here");
		if(intOne == 22)
			//intTwo and intThree used in calculation
			result = DoubleFusionTable [intTwo][intThree]; 
		else if(intTwo == 22)
			//intOne and intThree used in calculation
			result = DoubleFusionTable [intOne][intThree];
		else
			//intThree must be 0 and the first two ints used in calculation
			result = DoubleFusionTable [intOne][intTwo];
		return result;
	}
	
	//Calculate Triple Fusions
	int CalcTripleFusion(int intOne, int intTwo, int intThree, int [][] DoubleFusionTable, int [][] TripleFusionTable) {
		int result = 0, parsedHighestArc;
		String arcString;
		System.out.println("Please enter the highest level arcana. If it is a tie, please enter the arcana whose numerical value is lower.");
		//First find out from the user what the highest level arcana is
		Scanner highestArc = new Scanner(System.in);
		arcString = highestArc.next();
		parsedHighestArc = Integer.parseInt(arcString);
		if(parsedHighestArc != intOne & parsedHighestArc != intTwo & parsedHighestArc != intThree)
			System.out.println("Invalid highest level arcana entered. Exitting Persona 3 Portable Fusion Calculator.");
		//First Arcana is the highest lv persona
		else if(intOne == parsedHighestArc){
			result = DoubleFusionTable [intTwo][intThree];
			result = TripleFusionTable [result][intOne];
		}
		//Second Arcana is the highest lv persona
		else if(intTwo == parsedHighestArc){
			result = DoubleFusionTable [intOne][intThree];
			result = TripleFusionTable [result][intTwo];
		}
		//Third Arcana is the highest lv persona
		else {
			result = DoubleFusionTable [intOne][intTwo];
			result = TripleFusionTable [result][intThree];
		}
		
		return result;
	}
	
	//setTable initializes the double fusion database  
	int [][] SetDoubleFusionTable() {
		int [][] FusionTable = new int[22][22];
		//Currently cannot think of an efficient method of entering 
		//the data besides doing it manually.
		//-1 Denotes that fusing the two arcana is not possible.
		//Fool row start
		FusionTable [0][0] = 0;
		FusionTable [0][1] = 5;
		FusionTable [0][2] = 8;
		FusionTable [0][3] = 10;
		FusionTable [0][4] = 7;
		FusionTable [0][5] = 9;
		FusionTable [0][6] = 2;
		FusionTable [0][7] = 4;
		FusionTable [0][8] = 6;
		FusionTable [0][9] = 2;
		FusionTable [0][10] = 8;
		FusionTable [0][11] = 12;
		FusionTable [0][12] = 1;
		FusionTable [0][13] = 12;
		FusionTable [0][14] = 5;
		FusionTable [0][15] = 9;
		FusionTable [0][16] = 18;
		FusionTable [0][17] = 21;
		FusionTable [0][18] = 10;
		FusionTable [0][19] = 3;
		FusionTable [0][20] = 17;
		FusionTable [0][21] = 13;
		//Fool row end
		//Magician row start
		FusionTable [1][0] = 5;
		FusionTable [1][1] = 1;
		FusionTable [1][2] = 6;
		FusionTable [1][3] = 12;
		FusionTable [1][4] = 14;
		FusionTable [1][5] = 9;
		FusionTable [1][6] = 4;
		FusionTable [1][7] = 15;
		FusionTable [1][8] = 5;
		FusionTable [1][9] = 7;
		FusionTable [1][10] = 4;
		FusionTable [1][11] = -1;
		FusionTable [1][12] = 15;
		FusionTable [1][13] = -1;
		FusionTable [1][14] = 13;
		FusionTable [1][15] = 14;
		FusionTable [1][16] = 3;
		FusionTable [1][17] = 3;
		FusionTable [1][18] = 2;
		FusionTable [1][19] = 6;
		FusionTable [1][20] = -1;
		FusionTable [1][21] = -1;
		//Magician row end
		//Priestess row start
		FusionTable [2][0] = 8;
		FusionTable [2][1] = 6;
		FusionTable [2][2] = 2;
		FusionTable [2][3] = 6;
		FusionTable [2][4] = 8;
		FusionTable [2][5] = 7;
		FusionTable [2][6] = 1;
		FusionTable [2][7] = 1;
		FusionTable [2][8] = 6;
		FusionTable [2][9] = 11;
		FusionTable [2][10] = 1;
		FusionTable [2][11] = 9;
		FusionTable [2][12] = 11;
		FusionTable [2][13] = 4;
		FusionTable [2][14] = 3;
		FusionTable [2][15] = -1;
		FusionTable [2][16] = -1;
		FusionTable [2][17] = 8;
		FusionTable [2][18] = 17;
		FusionTable [2][19] = 17;
		FusionTable [2][20] = 3;
		FusionTable [2][21] = 3;
		//Priestess row end
		//Empress row start
		FusionTable [3][0] = 10;
		FusionTable [3][1] = 12;
		FusionTable [3][2] = 6;
		FusionTable [3][3] = 3;
		FusionTable [3][4] = 6;
		FusionTable [3][5] = 2;
		FusionTable [3][6] = 10;
		FusionTable [3][7] = 15;
		FusionTable [3][8] = 4;
		FusionTable [3][9] = 6;
		FusionTable [3][10] = 11;
		FusionTable [3][11] = 7;
		FusionTable [3][12] = 7;
		FusionTable [3][13] = 6;
		FusionTable [3][14] = 6;
		FusionTable [3][15] = 6;
		FusionTable [3][16] = 7;
		FusionTable [3][17] = 14;
		FusionTable [3][18] = 6;
		FusionTable [3][19] = 6;
		FusionTable [3][20] = -1;
		FusionTable [3][21] = 18;
		//Empress row end
		//Emperor row start
		FusionTable [4][0] = 7;
		FusionTable [4][1] = 14;
		FusionTable [4][2] = 8;
		FusionTable [4][3] = 6;
		FusionTable [4][4] = 4;
		FusionTable [4][5] = 7;
		FusionTable [4][6] = 7;
		FusionTable [4][7] = 9;
		FusionTable [4][8] = 15;
		FusionTable [4][9] = 11;
		FusionTable [4][10] = -1;
		FusionTable [4][11] = 12;
		FusionTable [4][12] = 9;
		FusionTable [4][13] = 18;
		FusionTable [4][14] = 12;
		FusionTable [4][15] = -1;
		FusionTable [4][16] = -1;
		FusionTable [4][17] = 8;
		FusionTable [4][18] = -1;
		FusionTable [4][19] = 3;
		FusionTable [4][20] = 5;
		FusionTable [4][21] = -1;
		//Emperor row end
		//Hierophant row start
		FusionTable [5][0] = 9;
		FusionTable [5][1] = 9;
		FusionTable [5][2] = 7;
		FusionTable [5][3] = 2;
		FusionTable [5][4] = 7;
		FusionTable [5][5] = 5;
		FusionTable [5][6] = 1;
		FusionTable [5][7] = 8;
		FusionTable [5][8] = 7;
		FusionTable [5][9] = 7;
		FusionTable [5][10] = 4;
		FusionTable [5][11] = 2;
		FusionTable [5][12] = 6;
		FusionTable [5][13] = 3;
		FusionTable [5][14] = 11;
		FusionTable [5][15] = -1;
		FusionTable [5][16] = 14;
		FusionTable [5][17] = 2;
		FusionTable [5][18] = 14;
		FusionTable [5][19] = 14;
		FusionTable [5][20] = 6;
		FusionTable [5][21] = -1;
		//Hierophant row end
		//Lovers row start
		FusionTable [6][0] = 2;
		FusionTable [6][1] = 4;
		FusionTable [6][2] = 1;
		FusionTable [6][3] = 10;
		FusionTable [6][4] = 7;
		FusionTable [6][5] = 1;
		FusionTable [6][6] = 6;
		FusionTable [6][7] = 4;
		FusionTable [6][8] = 7;
		FusionTable [6][9] = 8;
		FusionTable [6][10] = 1;
		FusionTable [6][11] = 5;
		FusionTable [6][12] = 9;
		FusionTable [6][13] = 15;
		FusionTable [6][14] = 2;
		FusionTable [6][15] = 11;
		FusionTable [6][16] = 17;
		FusionTable [6][17] = 5;
		FusionTable [6][18] = 3;
		FusionTable [6][19] = 5;
		FusionTable [6][20] = -1;
		FusionTable [6][21] = 12;
		//Lovers row end
		//Chariot row start
		FusionTable [7][0] = 4;
		FusionTable [7][1] = 15;
		FusionTable [7][2] = 1;
		FusionTable [7][3] = 15;
		FusionTable [7][4] = 9;
		FusionTable [7][5] = 8;
		FusionTable [7][6] = 4;
		FusionTable [7][7] = 7;
		FusionTable [7][8] = 1;
		FusionTable [7][9] = 14;
		FusionTable [7][10] = 11;
		FusionTable [7][11] = 8;
		FusionTable [7][12] = 10;
		FusionTable [7][13] = -1;
		FusionTable [7][14] = 15;
		FusionTable [7][15] = 12;
		FusionTable [7][16] = 18;
		FusionTable [7][17] = -1;
		FusionTable [7][18] = 10;
		FusionTable [7][19] = -1;
		FusionTable [7][20] = -1;
		FusionTable [7][21] = 15;
		//Chariot row end
		//Justice row start
		FusionTable [8][0] = 6;
		FusionTable [8][1] = 5;
		FusionTable [8][2] = 6;
		FusionTable [8][3] = 4;
		FusionTable [8][4] = 15;
		FusionTable [8][5] = 7;
		FusionTable [8][6] = 7;
		FusionTable [8][7] = 1;
		FusionTable [8][8] = 8;
		FusionTable [8][9] = 2;
		FusionTable [8][10] = 7;
		FusionTable [8][11] = 14;
		FusionTable [8][12] = 2;
		FusionTable [8][13] = 18;
		FusionTable [8][14] = 18;
		FusionTable [8][15] = -1;
		FusionTable [8][16] = 17;
		FusionTable [8][17] = 4;
		FusionTable [8][18] = -1;
		FusionTable [8][19] = 4;
		FusionTable [8][20] = 21;
		FusionTable [8][21] = -1;
		//Justice row end
		//Hermit row start
		FusionTable [9][0] = 2;
		FusionTable [9][1] = 7;
		FusionTable [9][2] = 11;
		FusionTable [9][3] = 6;
		FusionTable [9][4] = 11;
		FusionTable [9][5] = 7;
		FusionTable [9][6] = 8;
		FusionTable [9][7] = 14;
		FusionTable [9][8] = 2;
		FusionTable [9][9] = 9;
		FusionTable [9][10] = 4;
		FusionTable [9][11] = 10;
		FusionTable [9][12] = 10;
		FusionTable [9][13] = -1;
		FusionTable [9][14] = 12;
		FusionTable [9][15] = 13;
		FusionTable [9][16] = -1;
		FusionTable [9][17] = 7;
		FusionTable [9][18] = 1;
		FusionTable [9][19] = -1;
		FusionTable [9][20] = -1;
		FusionTable [9][21] = 17;
		//Hermit row end
		//Fortune row start
		FusionTable [10][0] = 8;
		FusionTable [10][1] = 4;
		FusionTable [10][2] = 1;
		FusionTable [10][3] = 11;
		FusionTable [10][4] = -1;
		FusionTable [10][5] = 4;
		FusionTable [10][6] = 1;
		FusionTable [10][7] = 11;
		FusionTable [10][8] = 7;
		FusionTable [10][9] = 4;
		FusionTable [10][10] = 10;
		FusionTable [10][11] = -1;
		FusionTable [10][12] = 11;
		FusionTable [10][13] = -1;
		FusionTable [10][14] = 6;
		FusionTable [10][15] = 18;
		FusionTable [10][16] = 18;
		FusionTable [10][17] = 18;
		FusionTable [10][18] = 7;
		FusionTable [10][19] = 14;
		FusionTable [10][20] = -1;
		FusionTable [10][21] = 15;
		//Fortune row end
		//Strength row start
		FusionTable [11][0] = 12;
		FusionTable [11][1] = -1;
		FusionTable [11][2] = 9;
		FusionTable [11][3] = 7;
		FusionTable [11][4] = 12;
		FusionTable [11][5] = 2;
		FusionTable [11][6] = 5;
		FusionTable [11][7] = 8;
		FusionTable [11][8] = 14;
		FusionTable [11][9] = 10;
		FusionTable [11][10] = -1;
		FusionTable [11][11] = 11;
		FusionTable [11][12] = 9;
		FusionTable [11][13] = 12;
		FusionTable [11][14] = 18;
		FusionTable [11][15] = 10;
		FusionTable [11][16] = 15;
		FusionTable [11][17] = 2;
		FusionTable [11][18] = 12;
		FusionTable [11][19] = 2;
		FusionTable [11][20] = 12;
		FusionTable [11][21] = -1;
		//Strength row end
		//Hanged Man row start
		FusionTable [12][0] = 1;
		FusionTable [12][1] = 15;
		FusionTable [12][2] = 11;
		FusionTable [12][3] = 7;
		FusionTable [12][4] = 9;
		FusionTable [12][5] = 6;
		FusionTable [12][6] = 9;
		FusionTable [12][7] = 10;
		FusionTable [12][8] = 2;
		FusionTable [12][9] = 10;
		FusionTable [12][10] = 11;
		FusionTable [12][11] = 9;
		FusionTable [12][12] = 12;
		FusionTable [12][13] = 15;
		FusionTable [12][14] = 5;
		FusionTable [12][15] = 18;
		FusionTable [12][16] = 13;
		FusionTable [12][17] = 11;
		FusionTable [12][18] = 3;
		FusionTable [12][19] = -1;
		FusionTable [12][20] = -1;
		FusionTable [12][21] = 14;
		//Hanged Man row end
		//Death row start
		FusionTable [13][0] = 11;
		FusionTable [13][1] = -1;
		FusionTable [13][2] = 4;
		FusionTable [13][3] = 6;
		FusionTable [13][4] = 18;
		FusionTable [13][5] = 3;
		FusionTable [13][6] = 15;
		FusionTable [13][7] = -1;
		FusionTable [13][8] = 18;
		FusionTable [13][9] = -1;
		FusionTable [13][10] = -1;
		FusionTable [13][11] = 12;
		FusionTable [13][12] = 15;
		FusionTable [13][13] = 13;
		FusionTable [13][14] = -1;
		FusionTable [13][15] = -1;
		FusionTable [13][16] = -1;
		FusionTable [13][17] = -1;
		FusionTable [13][18] = 17;
		FusionTable [13][19] = -1;
		FusionTable [13][20] = -1;
		FusionTable [13][21] = -1;
		//Death row end
		//Temperance row start
		FusionTable [14][0] = 5;
		FusionTable [14][1] = 13;
		FusionTable [14][2] = 3;
		FusionTable [14][3] = 6;
		FusionTable [14][4] = 12;
		FusionTable [14][5] = 11;
		FusionTable [14][6] = 2;
		FusionTable [14][7] = 13;
		FusionTable [14][8] = 18;
		FusionTable [14][9] = 12;
		FusionTable [14][10] = 6;
		FusionTable [14][11] = 18;
		FusionTable [14][12] = 5;
		FusionTable [14][13] = -1;
		FusionTable [14][14] = 14;
		FusionTable [14][15] = 13;
		FusionTable [14][16] = 15;
		FusionTable [14][17] = 18;
		FusionTable [14][18] = 3;
		FusionTable [14][19] = 17;
		FusionTable [14][20] = 18;
		FusionTable [14][21] = 17;
		//Temperance row end
		//Devil row start
		FusionTable [15][0] = 9;
		FusionTable [15][1] = 14;
		FusionTable [15][2] = -1;
		FusionTable [15][3] = 6;
		FusionTable [15][4] = -1;
		FusionTable [15][5] = -1;
		FusionTable [15][6] = 11;
		FusionTable [15][7] = 12;
		FusionTable [15][8] = -1;
		FusionTable [15][9] = 13;
		FusionTable [15][10] = 18;
		FusionTable [15][11] = 10;
		FusionTable [15][12] = 18;
		FusionTable [15][13] = -1;
		FusionTable [15][14] = 13;
		FusionTable [15][15] = 15;
		FusionTable [15][16] = -1;
		FusionTable [15][17] = -1;
		FusionTable [15][18] = -1;
		FusionTable [15][19] = -1;
		FusionTable [15][20] = -1;
		FusionTable [15][21] = 6;
		//Devil row end
		//Tower row start
		FusionTable [16][0] = 18;
		FusionTable [16][1] = 3;
		FusionTable [16][2] = -1;
		FusionTable [16][3] = 7;
		FusionTable [16][4] = -1;
		FusionTable [16][5] = 14;
		FusionTable [16][6] = 17;
		FusionTable [16][7] = 18;
		FusionTable [16][8] = 17;
		FusionTable [16][9] = -1;
		FusionTable [16][10] = 18;
		FusionTable [16][11] = 15;
		FusionTable [16][12] = 13;
		FusionTable [16][13] = -1;
		FusionTable [16][14] = 15;
		FusionTable [16][15] = -1;
		FusionTable [16][16] = 16;
		FusionTable [16][17] = -1;
		FusionTable [16][18] = 10;
		FusionTable [16][19] = -1;
		FusionTable [16][20] = 21;
		FusionTable [16][21] = 18;
		//Tower row end
		//Star row start
		FusionTable [17][0] = 21;
		FusionTable [17][1] = 3;
		FusionTable [17][2] = 8;
		FusionTable [17][3] = 14;
		FusionTable [17][4] = 8;
		FusionTable [17][5] = 2;
		FusionTable [17][6] = 5;
		FusionTable [17][7] = -1;
		FusionTable [17][8] = 4;
		FusionTable [17][9] = 7;
		FusionTable [17][10] = 18;
		FusionTable [17][11] = 2;
		FusionTable [17][12] = 11;
		FusionTable [17][13] = -1;
		FusionTable [17][14] = 18;
		FusionTable [17][15] = -1;
		FusionTable [17][16] = -1;
		FusionTable [17][17] = 17;
		FusionTable [17][18] = 13;
		FusionTable [17][19] = 8;
		FusionTable [17][20] = 14;
		FusionTable [17][21] = 15;
		//Star row end
		//Moon row start
		FusionTable [18][0] = 10;
		FusionTable [18][1] = 2;
		FusionTable [18][2] = 17;
		FusionTable [18][3] = 6;
		FusionTable [18][4] = -1;
		FusionTable [18][5] = 14;
		FusionTable [18][6] = 3;
		FusionTable [18][7] = 10;
		FusionTable [18][8] = -1;
		FusionTable [18][9] = 1;
		FusionTable [18][10] = 7;
		FusionTable [18][11] = 12;
		FusionTable [18][12] = 3;
		FusionTable [18][13] = 17;
		FusionTable [18][14] = 3;
		FusionTable [18][15] = -1;
		FusionTable [18][16] = 10;
		FusionTable [18][17] = 13;
		FusionTable [18][18] = 18;
		FusionTable [18][19] = 14;
		FusionTable [18][20] = -1;
		FusionTable [18][21] = -1;
		//Moon row end
		//Sun row start
		FusionTable [19][0] = 3;
		FusionTable [19][1] = 6;
		FusionTable [19][2] = 17;
		FusionTable [19][3] = 6;
		FusionTable [19][4] = 3;
		FusionTable [19][5] = 14;
		FusionTable [19][6] = 5;
		FusionTable [19][7] = -1;
		FusionTable [19][8] = 4;
		FusionTable [19][9] = -1;
		FusionTable [19][10] = 14;
		FusionTable [19][11] = 2;
		FusionTable [19][12] = -1;
		FusionTable [19][13] = -1;
		FusionTable [19][14] = 17;
		FusionTable [19][15] = -1;
		FusionTable [19][16] = -1;
		FusionTable [19][17] = 8;
		FusionTable [19][18] = 14;
		FusionTable [19][19] = 19;
		FusionTable [19][20] = 17;
		FusionTable [19][21] = 3;
		//Sun row end
		//Judgment row start
		FusionTable [20][0] = 17;
		FusionTable [20][1] = -1;
		FusionTable [20][2] = 3;
		FusionTable [20][3] = -1;
		FusionTable [20][4] = 5;
		FusionTable [20][5] = 6;
		FusionTable [20][6] = -1;
		FusionTable [20][7] = -1;
		FusionTable [20][8] = 21;
		FusionTable [20][9] = -1;
		FusionTable [20][10] = -1;
		FusionTable [20][11] = 12;
		FusionTable [20][12] = -1;
		FusionTable [20][13] = -1;
		FusionTable [20][14] = 18;
		FusionTable [20][15] = -1;
		FusionTable [20][16] = 21;
		FusionTable [20][17] = 14;
		FusionTable [20][18] = -1;
		FusionTable [20][19] = 17;
		FusionTable [20][20] = 20;
		FusionTable [20][21] = 17;
		//Judgement row end
		//Aeon row start
		FusionTable [21][0] = 13;
		FusionTable [21][1] = -1;
		FusionTable [21][2] = 3;
		FusionTable [21][3] = 18;
		FusionTable [21][4] = -1;
		FusionTable [21][5] = -1;
		FusionTable [21][6] = 12;
		FusionTable [21][7] = 13;
		FusionTable [21][8] = -1;
		FusionTable [21][9] = 17;
		FusionTable [21][10] = 15;
		FusionTable [21][11] = -1;
		FusionTable [21][12] = 14;
		FusionTable [21][13] = -1;
		FusionTable [21][14] = 17;
		FusionTable [21][15] = 6;
		FusionTable [21][16] = 18;
		FusionTable [21][17] = 15;
		FusionTable [21][18] = -1;
		FusionTable [21][19] = 3;
		FusionTable [21][20] = 17;
		FusionTable [21][21] = 21;
		//Aeon row end
		return FusionTable;
	}

	int [][] setTripleFusionTable(){
		int [][] FusionTable = new int [22][22];
		//Currently cannot think of an efficient method of entering 
		//the data besides doing it manually.
		//Fool row start
		FusionTable [0][0] = 0;
		FusionTable [0][1] = 4;
		FusionTable [0][2] = 1;
		FusionTable [0][3] = 10;
		FusionTable [0][4] = 7;
		FusionTable [0][5] = 9;
		FusionTable [0][6] = 14;
		FusionTable [0][7] = 15;
		FusionTable [0][8] = 1;
		FusionTable [0][9] = 2;
		FusionTable [0][10] = 8;
		FusionTable [0][11] = 12;
		FusionTable [0][12] = 1;
		FusionTable [0][13] = 11;
		FusionTable [0][14] = 11;
		FusionTable [0][15] = 8;
		FusionTable [0][16] = 18;
		FusionTable [0][17] = 21;
		FusionTable [0][18] = 10;
		FusionTable [0][19] = 3;
		FusionTable [0][20] = 17;
		FusionTable [0][21] = 13;
		//Fool row end
		//Magician row start
		FusionTable [1][0] = 4;
		FusionTable [1][1] = 1;
		FusionTable [1][2] = 15;
		FusionTable [1][3] = 12;
		FusionTable [1][4] = 6;
		FusionTable [1][5] = 9;
		FusionTable [1][6] = 15;
		FusionTable [1][7] = 18;
		FusionTable [1][8] = 0;
		FusionTable [1][9] = 12;
		FusionTable [1][10] = 4;
		FusionTable [1][11] = 17;
		FusionTable [1][12] = 15;
		FusionTable [1][13] = 16;
		FusionTable [1][14] = 13;
		FusionTable [1][15] = 14;
		FusionTable [1][16] = 3;
		FusionTable [1][17] = 3;
		FusionTable [1][18] = 10;
		FusionTable [1][19] = 6;
		FusionTable [1][20] = 16;
		FusionTable [1][21] = 19;
		//Magician row end
		//Priestess row start
		FusionTable [2][0] = 1;
		FusionTable [2][1] = 15;
		FusionTable [2][2] = 2;
		FusionTable [2][3] = 6;
		FusionTable [2][4] = 5;
		FusionTable [2][5] = 7;
		FusionTable [2][6] = 9;
		FusionTable [2][7] = 4;
		FusionTable [2][8] = 5;
		FusionTable [2][9] = 0;
		FusionTable [2][10] = 1;
		FusionTable [2][11] = 7;
		FusionTable [2][12] = 11;
		FusionTable [2][13] = 4;
		FusionTable [2][14] = 3;
		FusionTable [2][15] = 16;
		FusionTable [2][16] = 13;
		FusionTable [2][17] = 8;
		FusionTable [2][18] = 17;
		FusionTable [2][19] = 17;
		FusionTable [2][20] = 8;
		FusionTable [2][21] = 3;
		//Priestess row end
		//Empress row start
		FusionTable [3][0] = 10;
		FusionTable [3][1] = 12;
		FusionTable [3][2] = 6;
		FusionTable [3][3] = 3;
		FusionTable [3][4] = 0;
		FusionTable [3][5] = 2;
		FusionTable [3][6] = 10;
		FusionTable [3][7] = 15;
		FusionTable [3][8] = 4;
		FusionTable [3][9] = 6;
		FusionTable [3][10] = 11;
		FusionTable [3][11] = 7;
		FusionTable [3][12] = 7;
		FusionTable [3][13] = 15;
		FusionTable [3][14] = 6;
		FusionTable [3][15] = 1;
		FusionTable [3][16] = 7;
		FusionTable [3][17] = 14;
		FusionTable [3][18] = 6;
		FusionTable [3][19] = 6;
		FusionTable [3][20] = 15;
		FusionTable [3][21] = 18;
		//Empress row end
		//Emperor row start
		FusionTable [4][0] = 7;
		FusionTable [4][1] = 6;
		FusionTable [4][2] = 5;
		FusionTable [4][3] = 0;
		FusionTable [4][4] = 4;
		FusionTable [4][5] = 7;
		FusionTable [4][6] = 11;
		FusionTable [4][7] = 8;
		FusionTable [4][8] = 7;
		FusionTable [4][9] = 6;
		FusionTable [4][10] = 19;
		FusionTable [4][11] = 12;
		FusionTable [4][12] = 14;
		FusionTable [4][13] = 18;
		FusionTable [4][14] = 12;
		FusionTable [4][15] = 16;
		FusionTable [4][16] = 13;
		FusionTable [4][17] = 9;
		FusionTable [4][18] = 16;
		FusionTable [4][19] = 3;
		FusionTable [4][20] = 5;
		FusionTable [4][21] = 19;
		//Emperor row end
		//Hierophant row start
		FusionTable [5][0] = 9;
		FusionTable [5][1] = 9;
		FusionTable [5][2] = 7;
		FusionTable [5][3] = 2;
		FusionTable [5][4] = 7;
		FusionTable [5][5] = 5;
		FusionTable [5][6] = 14;
		FusionTable [5][7] = 19;
		FusionTable [5][8] = 1;
		FusionTable [5][9] = 7;
		FusionTable [5][10] = 4;
		FusionTable [5][11] = 4;
		FusionTable [5][12] = 10;
		FusionTable [5][13] = 3;
		FusionTable [5][14] = 11;
		FusionTable [5][15] = 0;
		FusionTable [5][16] = 14;
		FusionTable [5][17] = 2;
		FusionTable [5][18] = 14;
		FusionTable [5][19] = 14;
		FusionTable [5][20] = 6;
		FusionTable [5][21] = 14;
		//Hierophant row end
		//Lovers row start
		FusionTable [6][0] = 14;
		FusionTable [6][1] = 15;
		FusionTable [6][2] = 9;
		FusionTable [6][3] = 10;
		FusionTable [6][4] = 11;
		FusionTable [6][5] = 14;
		FusionTable [6][6] = 6;
		FusionTable [6][7] = 11;
		FusionTable [6][8] = 12;
		FusionTable [6][9] = 5;
		FusionTable [6][10] = 0;
		FusionTable [6][11] = 5;
		FusionTable [6][12] = 9;
		FusionTable [6][13] = 15;
		FusionTable [6][14] = 2;
		FusionTable [6][15] = 13;
		FusionTable [6][16] = 17;
		FusionTable [6][17] = 5;
		FusionTable [6][18] = 3;
		FusionTable [6][19] = 5;
		FusionTable [6][20] = 19;
		FusionTable [6][21] = 12;
		//Lovers row end
		//Chariot row start
		FusionTable [7][0] = 15;
		FusionTable [7][1] = 18;
		FusionTable [7][2] = 4;
		FusionTable [7][3] = 15;
		FusionTable [7][4] = 8;
		FusionTable [7][5] = 19;
		FusionTable [7][6] = 11;
		FusionTable [7][7] = 7;
		FusionTable [7][8] = 1;
		FusionTable [7][9] = 12;
		FusionTable [7][10] = 9;
		FusionTable [7][11] = 8;
		FusionTable [7][12] = 10;
		FusionTable [7][13] = 0;
		FusionTable [7][14] = 13;
		FusionTable [7][15] = 17;
		FusionTable [7][16] = 18;
		FusionTable [7][17] = 19;
		FusionTable [7][18] = 10;
		FusionTable [7][19] = 8;
		FusionTable [7][20] = 16;
		FusionTable [7][21] = 13;
		//Chariot row end
		//Justice row start
		FusionTable [8][0] = 1;
		FusionTable [8][1] = 0;
		FusionTable [8][2] = 5;
		FusionTable [8][3] = 4;
		FusionTable [8][4] = 7;
		FusionTable [8][5] = 1;
		FusionTable [8][6] = 12;
		FusionTable [8][7] = 1;
		FusionTable [8][8] = 8;
		FusionTable [8][9] = 11;
		FusionTable [8][10] = 7;
		FusionTable [8][11] = 14;
		FusionTable [8][12] = 2;
		FusionTable [8][13] = 18;
		FusionTable [8][14] = 18;
		FusionTable [8][15] = 16;
		FusionTable [8][16] = 19;
		FusionTable [8][17] = 4;
		FusionTable [8][18] = 16;
		FusionTable [8][19] = 4;
		FusionTable [8][20] = 21;
		FusionTable [8][21] = 20;
		//Justice row end
		//Hermit row start
		FusionTable [9][0] = 2;
		FusionTable [9][1] = 12;
		FusionTable [9][2] = 0;
		FusionTable [9][3] = 6;
		FusionTable [9][4] = 6;
		FusionTable [9][5] = 7;
		FusionTable [9][6] = 5;
		FusionTable [9][7] = 12;
		FusionTable [9][8] = 11;
		FusionTable [9][9] = 9;
		FusionTable [9][10] = 4;
		FusionTable [9][11] = 10;
		FusionTable [9][12] = 10;
		FusionTable [9][13] = 16;
		FusionTable [9][14] = 12;
		FusionTable [9][15] = 13;
		FusionTable [9][16] = 13;
		FusionTable [9][17] = 7;
		FusionTable [9][18] = 1;
		FusionTable [9][19] = 17;
		FusionTable [9][20] = 16;
		FusionTable [9][21] = 17;
		//Hermit row end
		//Fortune row start
		FusionTable [10][0] = 8;
		FusionTable [10][1] = 4;
		FusionTable [10][2] = 1;
		FusionTable [10][3] = 11;
		FusionTable [10][4] = 19;
		FusionTable [10][5] = 4;
		FusionTable [10][6] = 0;
		FusionTable [10][7] = 9;
		FusionTable [10][8] = 7;
		FusionTable [10][9] = 4;
		FusionTable [10][10] = 10;
		FusionTable [10][11] = 19;
		FusionTable [10][12] = 11;
		FusionTable [10][13] = 20;
		FusionTable [10][14] = 6;
		FusionTable [10][15] = 9;
		FusionTable [10][16] = 21;
		FusionTable [10][17] = 18;
		FusionTable [10][18] = 7;
		FusionTable [10][19] = 14;
		FusionTable [10][20] = 17;
		FusionTable [10][21] = 15;
		//Fortune row end
		//Strength row start
		FusionTable [11][0] = 12;
		FusionTable [11][1] = 17;
		FusionTable [11][2] = 7;
		FusionTable [11][3] = 7;
		FusionTable [11][4] = 12;
		FusionTable [11][5] = 4;
		FusionTable [11][6] = 5;
		FusionTable [11][7] = 8;
		FusionTable [11][8] = 14;
		FusionTable [11][9] = 10;
		FusionTable [11][10] = 19;
		FusionTable [11][11] = 11;
		FusionTable [11][12] = 10;
		FusionTable [11][13] = 12;
		FusionTable [11][14] = 18;
		FusionTable [11][15] = 3;
		FusionTable [11][16] = 20;
		FusionTable [11][17] = 2;
		FusionTable [11][18] = 21;
		FusionTable [11][19] = 2;
		FusionTable [11][20] = 12;
		FusionTable [11][21] = 16;
		//Strength row end
		//Hanged Man row start
		FusionTable [12][0] = 1;
		FusionTable [12][1] = 15;
		FusionTable [12][2] = 11;
		FusionTable [12][3] = 7;
		FusionTable [12][4] = 14;
		FusionTable [12][5] = 10;
		FusionTable [12][6] = 9;
		FusionTable [12][7] = 10;
		FusionTable [12][8] = 2;
		FusionTable [12][9] = 10;
		FusionTable [12][10] = 11;
		FusionTable [12][11] = 10;
		FusionTable [12][12] = 12;
		FusionTable [12][13] = 15;
		FusionTable [12][14] = 5;
		FusionTable [12][15] = 13;
		FusionTable [12][16] = 13;
		FusionTable [12][17] = 11;
		FusionTable [12][18] = 3;
		FusionTable [12][19] = 20;
		FusionTable [12][20] = 21;
		FusionTable [12][21] = 14;
		//Hanged Man row end
		//Death row start
		FusionTable [13][0] = 11;
		FusionTable [13][1] = 16;
		FusionTable [13][2] = 4;
		FusionTable [13][3] = 15;
		FusionTable [13][4] = 18;
		FusionTable [13][5] = 3;
		FusionTable [13][6] = 15;
		FusionTable [13][7] = 0;
		FusionTable [13][8] = 18;
		FusionTable [13][9] = 16;
		FusionTable [13][10] = 20;
		FusionTable [13][11] = 12;
		FusionTable [13][12] = 15;
		FusionTable [13][13] = 13;
		FusionTable [13][14] = 16;
		FusionTable [13][15] = 20;
		FusionTable [13][16] = 19;
		FusionTable [13][17] = 16;
		FusionTable [13][18] = 17;
		FusionTable [13][19] = 18;
		FusionTable [13][20] = 0;
		FusionTable [13][21] = 19;
		//Death row end
		//Temperance row start
		FusionTable [14][0] = 11;
		FusionTable [14][1] = 13;
		FusionTable [14][2] = 3;
		FusionTable [14][3] = 6;
		FusionTable [14][4] = 12;
		FusionTable [14][5] = 11;
		FusionTable [14][6] = 2;
		FusionTable [14][7] = 13;
		FusionTable [14][8] = 18;
		FusionTable [14][9] = 12;
		FusionTable [14][10] = 6;
		FusionTable [14][11] = 18;
		FusionTable [14][12] = 5;
		FusionTable [14][13] = 16;
		FusionTable [14][14] = 14;
		FusionTable [14][15] = 18;
		FusionTable [14][16] = 15;
		FusionTable [14][17] = 9;
		FusionTable [14][18] = 3;
		FusionTable [14][19] = 20;
		FusionTable [14][20] = 8;
		FusionTable [14][21] = 17;
		//Temperance row end
		//Devil row start
		FusionTable [15][0] = 8;
		FusionTable [15][1] = 14;
		FusionTable [15][2] = 16;
		FusionTable [15][3] = 1;
		FusionTable [15][4] = 16;
		FusionTable [15][5] = 0;
		FusionTable [15][6] = 13;
		FusionTable [15][7] = 17;
		FusionTable [15][8] = 16;
		FusionTable [15][9] = 13;
		FusionTable [15][10] = 9;
		FusionTable [15][11] = 3;
		FusionTable [15][12] = 13;
		FusionTable [15][13] = 20;
		FusionTable [15][14] = 18;
		FusionTable [15][15] = 15;
		FusionTable [15][16] = 20;
		FusionTable [15][17] = 1;
		FusionTable [15][18] = 20;
		FusionTable [15][19] = 13;
		FusionTable [15][20] = 18;
		FusionTable [15][21] = 6;
		//Devil row end
		//Tower row start
		FusionTable [16][0] = 18;
		FusionTable [16][1] = 3;
		FusionTable [16][2] = 13;
		FusionTable [16][3] = 7;
		FusionTable [16][4] = 13;
		FusionTable [16][5] = 14;
		FusionTable [16][6] = 17;
		FusionTable [16][7] = 18;
		FusionTable [16][8] = 19;
		FusionTable [16][9] = 13;
		FusionTable [16][10] = 21;
		FusionTable [16][11] = 20;
		FusionTable [16][12] = 13;
		FusionTable [16][13] = 19;
		FusionTable [16][14] = 15;
		FusionTable [16][15] = 20;
		FusionTable [16][16] = 16;
		FusionTable [16][17] = 20;
		FusionTable [16][18] = 10;
		FusionTable [16][19] = 18;
		FusionTable [16][20] = 21;
		FusionTable [16][21] = 0;
		//Tower row end
		//Star row start
		FusionTable [17][0] = 21;
		FusionTable [17][1] = 3;
		FusionTable [17][2] = 8;
		FusionTable [17][3] = 14;
		FusionTable [17][4] = 9;
		FusionTable [17][5] = 2;
		FusionTable [17][6] = 5;
		FusionTable [17][7] = 19;
		FusionTable [17][8] = 4;
		FusionTable [17][9] = 7;
		FusionTable [17][10] = 18;
		FusionTable [17][11] = 2;
		FusionTable [17][12] = 11;
		FusionTable [17][13] = 16;
		FusionTable [17][14] = 9;
		FusionTable [17][15] = 1;
		FusionTable [17][16] = 20;
		FusionTable [17][17] = 17;
		FusionTable [17][18] = 19;
		FusionTable [17][19] = 21;
		FusionTable [17][20] = 14;
		FusionTable [17][21] = 15;
		//Star row end
		//Moon row start
		FusionTable [18][0] = 10;
		FusionTable [18][1] = 10;
		FusionTable [18][2] = 17;
		FusionTable [18][3] = 6;
		FusionTable [18][4] = 16;
		FusionTable [18][5] = 14;
		FusionTable [18][6] = 3;
		FusionTable [18][7] = 10;
		FusionTable [18][8] = 16;
		FusionTable [18][9] = 1;
		FusionTable [18][10] = 7;
		FusionTable [18][11] = 21;
		FusionTable [18][12] = 3;
		FusionTable [18][13] = 17;
		FusionTable [18][14] = 3;
		FusionTable [18][15] = 20;
		FusionTable [18][16] = 10;
		FusionTable [18][17] = 19;
		FusionTable [18][18] = 18;
		FusionTable [18][19] = 14;
		FusionTable [18][20] = 2;
		FusionTable [18][21] = 20;
		//Moon row end
		//Sun row start
		FusionTable [19][0] = 3;
		FusionTable [19][1] = 6;
		FusionTable [19][2] = 17;
		FusionTable [19][3] = 6;
		FusionTable [19][4] = 3;
		FusionTable [19][5] = 14;
		FusionTable [19][6] = 5;
		FusionTable [19][7] = 8;
		FusionTable [19][8] = 4;
		FusionTable [19][9] = 17;
		FusionTable [19][10] = 14;
		FusionTable [19][11] = 2;
		FusionTable [19][12] = 20;
		FusionTable [19][13] = 18;
		FusionTable [19][14] = 20;
		FusionTable [19][15] = 13;
		FusionTable [19][16] = 18;
		FusionTable [19][17] = 21;
		FusionTable [19][18] = 14;
		FusionTable [19][19] = 19;
		FusionTable [19][20] = 17;
		FusionTable [19][21] = 3;
		//Sun row end
		//Judgment row start
		FusionTable [20][0] = 17;
		FusionTable [20][1] = 16;
		FusionTable [20][2] = 8;
		FusionTable [20][3] = 15;
		FusionTable [20][4] = 5;
		FusionTable [20][5] = 6;
		FusionTable [20][6] = 19;
		FusionTable [20][7] = 16;
		FusionTable [20][8] = 21;
		FusionTable [20][9] = 16;
		FusionTable [20][10] = 17;
		FusionTable [20][11] = 12;
		FusionTable [20][12] = 21;
		FusionTable [20][13] = 0;
		FusionTable [20][14] = 8;
		FusionTable [20][15] = 18;
		FusionTable [20][16] = 21;
		FusionTable [20][17] = 14;
		FusionTable [20][18] = 2;
		FusionTable [20][19] = 17;
		FusionTable [20][20] = 20;
		FusionTable [20][21] = 0;
		//Judgement row end
		//Aeon row start
		FusionTable [21][0] = 13;
		FusionTable [21][1] = 19;
		FusionTable [21][2] = 3;
		FusionTable [21][3] = 18;
		FusionTable [21][4] = 19;
		FusionTable [21][5] = 14;
		FusionTable [21][6] = 12;
		FusionTable [21][7] = 13;
		FusionTable [21][8] = 20;
		FusionTable [21][9] = 17;
		FusionTable [21][10] = 15;
		FusionTable [21][11] = 16;
		FusionTable [21][12] = 14;
		FusionTable [21][13] = 19;
		FusionTable [21][14] = 17;
		FusionTable [21][15] = 6;
		FusionTable [21][16] = 0;
		FusionTable [21][17] = 15;
		FusionTable [21][18] = 20;
		FusionTable [21][19] = 3;
		FusionTable [21][20] = 0;
		FusionTable [21][21] = 21;
		//Aeon row end
		return FusionTable;
	}
}


